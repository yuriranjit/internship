from flask import render_template, request, redirect

from web_app import app, db
from modles import Blogs


@app.route('/')
def blog_page():
    datas = Blogs.query.order_by(Blogs.id).all()
    return render_template('index.html', datas=datas)
    
@app.route('/save_data/', methods=['GET','POST'])
def save_data():
    if request.method == 'POST':
        username = request.form['username']
        blog_content = request.form['blog_content']
        
        data = {
            'username': username,
            'blog_content':blog_content
        }
        blog =Blogs(**data)
        
        db.session.add(blog)
        db.session.commit()
        return redirect('/')

@app.route('/content/')
def new_content():
    return render_template('content.html')
