from web_app import db


class Blogs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    blog_content = db.Column(db.String, nullable=False)
    